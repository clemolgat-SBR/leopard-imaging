//! @file
#pragma once

#include <Camera.hpp>
#include <QWidget>

class QPushButton;
class RegisterBox;

//! @brief Provides Camera Exposure Metering Testing view.
class ExposureMeteringControls : public QWidget {
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	ExposureMeteringControls(const ExposureMeteringControls&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	ExposureMeteringControls& operator=(const ExposureMeteringControls&) = delete;
	//! @brief Default Constructor.
	//! @param[in] camera Camera object to bind.
	//! @param[in] parent Parent Widget if any.
	ExposureMeteringControls(Camera& camera, QWidget* parent = 0);

	virtual ~ExposureMeteringControls() = default;

	public slots:
	//! @brief Reads data from device and upddate view.
	void onReadRegister();
	//! @brief Write data to device.
	void onWriteRegister();

	protected:
	//! @brief Enable the exposure windowing.
	//! @details register 0x501D [4]: Enable Windowing.
	RegisterBox* _regEnable; // 1

	//! @brief Stores the window start X (high byte)
	//! @details Register 0x5680 [3:0]: Start X [11:8]
	RegisterBox* _regXStartHigh; // 2
	//! @brief Stores the window start X (low byte)
	//! @details Register 0x5681 [7:0]: Start X [7:0]
	RegisterBox* _regXStartLow; // 3

	//! @brief Stores the window start Y (high byte)
	//! @details Register 0x5682 [2:0]: Start X [10:8]
	RegisterBox* _regYStartHigh; // 4
	//! @brief Stores the window start Y (low byte)
	//! @details Register 0x5683 [7:0]: Start X [7:0]
	RegisterBox* _regYStartLow; // 5

	//! @brief Stores the window X axis size (high byte)
	//! @details Register 0x5684 [3:0]: Window size X [11:8]
	RegisterBox* _regXWindowHigh; // 6
	//! @brief Stores the window X axis size (low byte)
	//! @details Register 0x5685 [7:0]: Window size X [7:0]
	RegisterBox* _regXWindowLow; // 7

	//! @brief Stores the window Y axis size (high byte)
	//! @details Register 0x5686 [2:0]: Window size Y [10:8]
	RegisterBox* _regYWindowHigh; // 8
	//! @brief Stores the window Y axis size (low byte)
	//! @details Register 0x5687 [7:0]: Window size Y [7:0]
	RegisterBox* _regYWindowLow; // 9

	//! @brief Store the weight 00 and the weight 01
	//! @details Register 0x5688 [7:0]: Weight00 [3:0] Weight01 [7:4]
	RegisterBox* _regWeight00_01; // 10
	//! @brief Store the weight 02 and the weight 03
	//! @details Register 0x5689 [7:0]: Weight02 [3:0] Weight03 [7:4]
	RegisterBox* _regWeight02_03; // 11

	//! @brief Store the weight 10 and the weight 11
	//! @details Register 0x568A [7:0]: Weight10 [3:0] Weight11 [7:4]
	RegisterBox* _regWeight10_11; // 12
	//! @brief Store the weight 12 and the weight 13
	//! @details Register 0x568B [7:0]: Weight12 [3:0] Weight13 [7:4]
	RegisterBox* _regWeight12_13; // 13

	//! @brief Store the weight 20 and the weight 21
	//! @details Register 0x568C [7:0]: Weight20 [3:0] Weight21 [7:4]
	RegisterBox* _regWeight20_21; // 14
	//! @brief Store the weight 22 and the weight 23
	//! @details Register 0x568D [7:0]: Weight22 [3:0] Weight23 [7:4]
	RegisterBox* _regWeight22_23; // 15

	//! @brief Store the weight 30 and the weight 31
	//! @details Register 0x568E [7:0]: Weight30 [3:0] Weight31 [7:4]
	RegisterBox* _regWeight30_31; // 16
	//! @brief Store the weight 32 and the weight 33
	//! @details Register 0x568F [7:0]: Weight32 [3:0] Weight33 [7:4]
	RegisterBox* _regWeight32_33; // 17

	//! @brief Stores read button widget.
	QPushButton* _regReadButton;
	//! @brief Stores write button widget.
	QPushButton* _regWriteButton;

	private:
	//! @brief Stores the Camera instance.
	Camera& _camera;

	private:
	//! @brief Initializes the widget.
	void setupGUI();
};
