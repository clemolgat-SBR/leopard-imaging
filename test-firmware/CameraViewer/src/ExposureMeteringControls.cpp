//! @file
#include "ExposureMeteringControls.hpp"

#include "RegisterBox.hpp"
#include <CameraLIOV5640.hpp>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

ExposureMeteringControls::ExposureMeteringControls(Camera& camera, QWidget* parent)
  : QWidget(parent)
  , _camera(camera) {
	setupGUI();
}

void
ExposureMeteringControls::onReadRegister() {
	try {
		std::array<std::uint8_t, 17> values =
		  dynamic_cast<CameraLIOV5640&>(_camera).readExposureMetering();

		_regEnable->setValue(values[0]);

		_regXStartHigh->setValue(values[1]);
		_regXStartLow->setValue(values[2]);
		_regYStartHigh->setValue(values[3]);
		_regYStartLow->setValue(values[4]);

		_regXWindowHigh->setValue(values[5]);
		_regXWindowLow->setValue(values[6]);
		_regYWindowHigh->setValue(values[7]);
		_regYWindowLow->setValue(values[8]);

		_regWeight00_01->setValue(values[9]);
		_regWeight02_03->setValue(values[10]);
		_regWeight10_11->setValue(values[11]);
		_regWeight12_13->setValue(values[12]);
		_regWeight20_21->setValue(values[13]);
		_regWeight22_23->setValue(values[14]);
		_regWeight30_31->setValue(values[15]);
		_regWeight32_33->setValue(values[16]);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::readRegister Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
ExposureMeteringControls::onWriteRegister() {
	try {
		std::array<std::uint8_t, 17> values;
		values[0] = _regEnable->value();

		values[1] = _regXStartHigh->value();
		values[2] = _regXStartLow->value();
		values[3] = _regYStartHigh->value();
		values[4] = _regYStartLow->value();

		values[5] = _regXWindowHigh->value();
		values[6] = _regXWindowLow->value();
		values[7] = _regYWindowHigh->value();
		values[8] = _regYWindowLow->value();

		values[9]  = _regWeight00_01->value();
		values[10] = _regWeight02_03->value();
		values[11] = _regWeight10_11->value();
		values[12] = _regWeight12_13->value();
		values[13] = _regWeight20_21->value();
		values[14] = _regWeight22_23->value();
		values[15] = _regWeight30_31->value();
		values[16] = _regWeight32_33->value();
		dynamic_cast<CameraLIOV5640&>(_camera).writeExposureMetering(values);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::writeRegister Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
ExposureMeteringControls::setupGUI() {
	setObjectName(QString::fromUtf8("ExposureMetering"));
	setLayout(new QVBoxLayout(this));

	// Enable
	_regEnable = new RegisterBox("Enable[4]");
	layout()->addWidget(_regEnable);

	// Start
	_regXStartHigh = new RegisterBox("XStart[11:8]");
	layout()->addWidget(_regXStartHigh);
	_regXStartLow = new RegisterBox("XStart[7:0]");
	layout()->addWidget(_regXStartLow);

	_regYStartHigh = new RegisterBox("YStart[10:8]");
	layout()->addWidget(_regYStartHigh);
	_regYStartLow = new RegisterBox("YStart[7:0]");
	layout()->addWidget(_regYStartLow);

	// Window
	_regXWindowHigh = new RegisterBox("XWindow[11:8]");
	layout()->addWidget(_regXWindowHigh);
	_regXWindowLow = new RegisterBox("XWindow[7:0]");
	layout()->addWidget(_regXWindowLow);

	_regYWindowHigh = new RegisterBox("YWindow[10:8]");
	layout()->addWidget(_regYWindowHigh);
	_regYWindowLow = new RegisterBox("YWindow[7:0]");
	layout()->addWidget(_regYWindowLow);

	// Weight
	_regWeight00_01 = new RegisterBox("Weight00[3:0] Weight01[7:4]");
	layout()->addWidget(_regWeight00_01);
	_regWeight02_03 = new RegisterBox("Weight02[3:0] Weight03[7:4]");
	layout()->addWidget(_regWeight02_03);

	_regWeight10_11 = new RegisterBox("Weight10[3:0] Weight11[7:4]");
	layout()->addWidget(_regWeight10_11);
	_regWeight12_13 = new RegisterBox("Weight12[3:0] Weight13[7:4]");
	layout()->addWidget(_regWeight12_13);

	_regWeight20_21 = new RegisterBox("Weight20[3:0] Weight21[7:4]");
	layout()->addWidget(_regWeight20_21);
	_regWeight22_23 = new RegisterBox("Weight22[3:0] Weight23[7:4]");
	layout()->addWidget(_regWeight22_23);

	_regWeight30_31 = new RegisterBox("Weight30[3:0] Weight31[7:4]");
	layout()->addWidget(_regWeight30_31);
	_regWeight32_33 = new RegisterBox("Weight32[3:0] Weight33[7:4]");
	layout()->addWidget(_regWeight32_33);

	QSpacerItem* spacer =
	  new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	layout()->addItem(spacer);

	// Button line
	{
		QHBoxLayout* buttonLayout = new QHBoxLayout();
		dynamic_cast<QVBoxLayout*>(layout())->addLayout(buttonLayout);

		_regReadButton = new QPushButton();
		_regReadButton->setText("Read");
		connect(_regReadButton,
		        &QPushButton::clicked,
		        this,
		        &ExposureMeteringControls::onReadRegister);
		buttonLayout->addWidget(_regReadButton);

		_regWriteButton = new QPushButton();
		_regWriteButton->setText("Write");
		connect(_regWriteButton,
		        &QPushButton::clicked,
		        this,
		        &ExposureMeteringControls::onWriteRegister);
		buttonLayout->addWidget(_regWriteButton);
	}
}
