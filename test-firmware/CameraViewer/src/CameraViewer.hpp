//! @file
#pragma once

#include <Camera.hpp>
#include <QImage>
#include <QLabel>
#include <QScrollArea>
#include <QSpinBox>
#include <QThread>

class Worker;
class ImageViewer;

//! @brief Display Kernel Image Buffer.
class CameraViewer : public QWidget {
	//! @brief Worker need access to CameraViewer.
	friend class Worker;
	Q_OBJECT
	public:
	//! @brief Remove copy constructor.
	CameraViewer(const CameraViewer&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	CameraViewer& operator=(const CameraViewer&) = delete;
	//! @brief Default Constructor.
	//! @param[in] camera Camera object to bind.
	//! @param[in] parent Parent Widget if any.
	CameraViewer(Camera& camera, QWidget* parent = 0);

	virtual ~CameraViewer() = default;

	public slots:
	//! @brief Start Viewer.
	void onStart();
	//! @brief Stop Viewer.
	void onStop();

	private slots:
	//! @brief Display error message.
	//! @param[in] txt Error to display.
	void onError(QString txt);

	protected:
	//! @brief Stores the Camera instance.
	Camera& _camera;
	//! @brief Stores the worker state.
	bool _isStarted;
	//! @brief Stores the worker instance.
	Worker* _worker;
	//! @brief Stores the ImageViewer widget.
	ImageViewer* _imageViewer;

	private:
	//! @brief Initializes the widget.
	void setupGUI();
};

//! @brief Worker to grabe Image from Camera.
class Worker : public QThread {
	Q_OBJECT
	signals:
	//! @brief Signal emited when new Image available.
	//! @param[in] image The new Image available.
	void image(QImage image);
	//! @brief Signal emited when worker can't get Image.
	//! @param[in] txt The error message to display.
	void error(QString txt);

	public:
	//! @brief Remove copy constructor.
	Worker(const Worker&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	Worker& operator=(const Worker&) = delete;
	//! @brief Default Constructor.
	//! @param[in] widget Parent widget.
	Worker(CameraViewer* widget)
	  : _widget(widget) {}
	//! @brief Starts grabing Image.
	void run();

	protected:
	//! @brief Gets a new Imaage from the Camera.
	//! @return the new Image available.
	QImage getImage();

	protected:
	//! @brief Store the CameraViewer parent widget.
	CameraViewer* _widget;
};
