//! @file
#pragma once

#include "CameraControls.hpp"
#include "CameraViewer.hpp"
#include <CameraLIOV5640.hpp>
#include <QPushButton>
#include <QWidget>

/// @brief Main Window Widget.
class CameraGUI : public QWidget {
	Q_OBJECT

	public:
	//! @brief Remove copy constructor.
	CameraGUI(const CameraGUI&) = delete;
	//! @brief Remove copy operator.
	//! @return *this.
	CameraGUI& operator=(const CameraGUI&) = delete;
	//! @brief Default Constructor.
	//! @param[in] device Name of the video device file.
	//! @param[in] parent Parent Widget if any.
	CameraGUI(const std::string& device, QWidget* parent = 0);

	//! @brief Destructor.
	virtual ~CameraGUI() = default;

	protected slots:
	//! @brief Opens Camera and updates view.
	void onOpenCamera();
	//! @brief Configure Camera Format and updates view.
	void onConfigureCamera();
	//! @brief Starts Camera streaming and updates view.
	void onStartCamera();
	//! @brief Stops Camera streaming and updates view.
	void onStopCamera();
	//! @brief Closes Camera and updates view.
	void onCloseCamera();
	//! @brief Quits the GUI.
	void slotQuit(void);

	protected:
	//! @brief Stores the Camera instance.
	CameraLIOV5640 _camera;
	//! @brief Stores the CameraViewer view.
	CameraViewer* _cameraViewer;
	//! @brief Stores the CameraControls view.
	CameraControls* _cameraControls;

	//! @brief Stores open button widget.
	QPushButton* _openButton;
	//! @brief Stores configure button widget.
	QPushButton* _configureButton;
	//! @brief Stores start button widget.
	QPushButton* _startButton;
	//! @brief Stores stop button widget.
	QPushButton* _stopButton;
	//! @brief Stores close button widget.
	QPushButton* _closeButton;

	private:
	//! @brief Initializes the widget.
	void setupGUI();
};
