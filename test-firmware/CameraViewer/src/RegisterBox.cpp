//! @file
#include "RegisterBox.hpp"

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

RegisterBox::RegisterBox(QString title, QWidget* parent)
  : QWidget(parent)
  , _title()
  , _HexValue()
  , _DecValue() {
	setupGUI(title);
}

int
RegisterBox::value() const {
	return _DecValue->value();
}

void
RegisterBox::setValue(int value) {
	_HexValue->setValue(value);
	_DecValue->setValue(value);
}

void
RegisterBox::setupGUI(QString title) {
	setObjectName(QString::fromUtf8("Register") + title);
	setLayout(new QHBoxLayout(this));

	_title = new QLabel();
	_title->setText(title + QString(": "));
	layout()->addWidget(_title);

	_HexValue = new QSpinBox();
	_HexValue->setPrefix("0x");
	_HexValue->setDisplayIntegerBase(16);
	_HexValue->setRange(0, 0x9999);
	_HexValue->setValue(0);
	layout()->addWidget(_HexValue);

	_DecValue = new QSpinBox();
	_DecValue->setRange(0, 0x9999);
	_DecValue->setValue(0);
	layout()->addWidget(_DecValue);

	QObject::connect(_HexValue,
	                 static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	                 _DecValue,
	                 &QSpinBox::setValue);
	connect(_DecValue,
	        static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        _HexValue,
	        &QSpinBox::setValue);
}
