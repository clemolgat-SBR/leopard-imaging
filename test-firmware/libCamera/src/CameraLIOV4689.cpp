//! @file
#include <CameraLIOV4689.hpp>

#include <chrono>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <thread>

extern "C" {
#include <linux/usb/video.h> // UVC Controls
#include <linux/uvcvideo.h>  // UVC ExtUnit Controls
#include <linux/videodev2.h> // V4L2 Controls
#include <sys/ioctl.h>       // ::ioctl
}

std::uint32_t
CameraLIOV4689::getVersion() const {
	/*
	// Version of the Base OV580.
	std::uint8_t value[384];
	std::memset(value, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = value;
	value[0]          = 0x51;
	value[1]          = 0x02;

	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query)) {
	  std::cerr << "(ERROR) " << _device
	            << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
	  throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(5));
	xu_query.query = UVC_GET_CUR;
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query)) {
	  std::cerr << "(ERROR) " << _device
	            << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
	  throw std::runtime_error(_device + ": UVC_GET_CUR fails: " + std::strerror(errno));
	}
	// DataSheet: Version Byte 16~19 but need to add the 1Byte shift.
	return std::uint32_t(value[20] << 24 | value[19] << 16 | value[18] << 8 | value[17]);
	*/

	// Version of Leopard Imaging OV580
	std::uint8_t low  = readISPRegister(0x8018fff0);
	std::uint8_t high = readISPRegister(0x8018fff1);
	return (high << 8) | low;
}

void
CameraLIOV4689::init() {
	CameraV4L2::init();

	setParameter(V4L2_CID_BRIGHTNESS, 4);
	setParameter(V4L2_CID_CONTRAST, 4);
	setParameter(V4L2_CID_SATURATION, 4);
	setParameter(V4L2_CID_HUE, 0);
	setParameter(V4L2_CID_HUE_AUTO, 1);
	setParameter(V4L2_CID_AUTO_WHITE_BALANCE, 1);

	// Disable Test Pattern
	writeSensorRegister(Sensor::SCCB0, 0x5040, 0x0);
	writeSensorRegister(Sensor::SCCB1, 0x5040, 0x0);
}

std::ostream&
operator<<(std::ostream& os, CameraLIOV4689::Sensor sensor) {
	switch (sensor) {
		case CameraLIOV4689::Sensor::SCCB0:
			os << "SCCB0";
			break;
		case CameraLIOV4689::Sensor::SCCB1:
			os << "SCCB1";
			break;
	}
	return os;
}

std::uint8_t
CameraLIOV4689::readSensorRegister(Sensor sensor, std::uint16_t address) const {
	std::uint8_t data[384];
	std::memset(data, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = data;
	data[0]           = 0x51;
	switch (sensor) {
		case Sensor::SCCB0:
			data[1] = 0xa3; // SCCB0
			break;
		case Sensor::SCCB1:
			data[1] = 0xa5; // SCCB1
			break;
	}
	data[2] = 0x6c;
	data[3] = 0x02; // 16 bits address
	data[4] = 0x01; // data with 1 Byte
	// register address (Big Endian)
	data[5] = 0x00;
	data[6] = 0x00;
	data[7] = address >> 8;
	data[8] = address & 0xff;

	data[9]  = 0x90;
	data[10] = 0x01;
	data[11] = 0x00;
	data[12] = 0x01;

	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(10));

	xu_query.query = UVC_GET_CUR; // UVC_GET_LEN UVC_GET_CUR
	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_CUR fails: " + std::strerror(errno));
	}
	return data[17];
}

void
CameraLIOV4689::writeSensorRegister(Sensor sensor,
                                    std::uint16_t address,
                                    std::uint8_t value) {
	std::uint8_t data[384];
	std::memset(data, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = data;
	data[0]           = 0x50;
	switch (sensor) {
		case Sensor::SCCB0:
			data[1] = 0xa3; // SCCB0
			break;
		case Sensor::SCCB1:
			data[1] = 0xa5; // SCCB1
			break;
	}
	data[2] = 0x6c;
	data[3] = 0x02; // 16bit address
	data[4] = 0x01; // data 1 byte
	// register address (Big Endian)
	data[5] = 0x00;
	data[6] = 0x00;
	data[7] = address >> 8;
	data[8] = address & 0xff;

	data[9]  = 0x90;
	data[10] = 0x01;
	data[11] = 0x00;
	data[12] = 0x01;

	data[16] = value;

	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
}

std::uint8_t
CameraLIOV4689::readISPRegister(std::uint32_t address) const {
	std::uint8_t data[384];
	std::memset(data, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = data;
	data[0]           = 0x51;
	data[1]           = 0xa2;
	data[2]           = 0x6c;
	data[3]           = 0x04;
	data[4]           = 0x01;
	// register address (Big Endian)
	data[5] = address >> 24;
	data[6] = address >> 16;
	data[7] = address >> 8;
	data[8] = address & 0xff;

	data[9]  = 0x90;
	data[10] = 0x01;
	data[11] = 0x00;
	data[12] = 0x01;

	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(10));

	xu_query.query = UVC_GET_CUR; // UVC_GET_LEN UVC_GET_CUR
	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_CUR fails: " + std::strerror(errno));
	}
	return data[17];
}

void
CameraLIOV4689::writeISPRegister(std::uint32_t address, std::uint8_t value) {
	std::uint8_t data[384];
	std::memset(data, 0, 384);

	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 4;
	xu_query.selector = 0x02;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 384;
	xu_query.data     = data;
	data[0]           = 0x50;
	data[1]           = 0xa2;
	data[2]           = 0x6c;
	data[3]           = 0x04;
	data[4]           = 0x01;
	// register address (Big Endian)
	data[5] = address >> 24;
	data[6] = address >> 16;
	data[7] = address >> 8;
	data[8] = address & 0xff;

	data[9]  = 0x90;
	data[10] = 0x01;
	data[11] = 0x00;
	data[12] = 0x01;

	data[16] = value;

	if (ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query) != 0) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
}
