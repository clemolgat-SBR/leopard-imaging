//! @file
#include <Camera.hpp>

#include <algorithm>
#include <fstream> // std::ofstream
#include <iostream>
#include <sstream>
#include <stdexcept>

Camera::Camera(std::string device, Format format, bool verbose)
  : _device(std::move(device))
  , _format(std::move(format))
  , _ringBufferSize(4)
  , _enableVerbose(verbose) {}

void
Camera::setFormat(const Camera::Format& format, bool bypassCheck) {
	_format = format;
	_updateDeviceResolution(bypassCheck);
	_updateDeviceFrameRate(bypassCheck);
}

void
Camera::setRingBufferSize(std::uint32_t bufferNumber) {
	if (_ringBufferSize == bufferNumber) return;

	_ringBufferSize = bufferNumber;
	_updateRingBufferSize();
}

//! @brief Clamp pixel value in the range [0,255]
//! @param[in] v Pixel value to clamp.
//! @return The value clamped in the range [0,255].
static inline std::uint8_t
clamp(int v) {
	return std::uint8_t(std::uint32_t(v) <= 255 ? v : v > 0 ? 255 : 0);
}

//! @brief convert YUV color space tuple to RGB color space.
//! @param[in] y Y channel of the input.
//! @param[in] u U channel of the input.
//! @param[in] v V channel of the input.
//! @param[out] r Red channel of the output.
//! @param[out] g Green channel of the output.
//! @param[out] b Blue channel of the output.
static inline void
yuv2rgb(const int y,
        const int u,
        const int v,
        unsigned char& r,
        unsigned char& g,
        unsigned char& b) {
	int c, d, e;
	c = y - 16;
	d = u - 128;
	e = v - 128;
	r = clamp((298 * c + 409 * e + 128) >> 8);
	g = clamp((298 * c - 100 * d - 208 * e + 128) >> 8);
	b = clamp((298 * c + 516 * d + 128) >> 8);
}

void
Camera::writeImage2PPM(const Image& image, const std::string& fileName) {
	std::ofstream ofs;
	ofs.open(fileName, std::ofstream::out | std::ios_base::binary);
	if (!ofs.is_open()) {
		throw std::runtime_error("Can't create file " + fileName);
	}
	ofs << "P6" << std::endl
	    << image.width << " " << image.height << std::endl
	    << "255" << std::endl;
	const std::uint8_t* data = image.buffer.get();
	for (std::uint32_t j = 0; j < image.height; ++j) {
		for (std::uint32_t i = 0; i < image.width; i += 2) {
			std::uint8_t y0 = *data++;
			std::uint8_t u  = *data++;
			std::uint8_t y1 = *data++;
			std::uint8_t v  = *data++;

			std::uint8_t red, green, blue;
			yuv2rgb(y0, u, v, red, green, blue);
			ofs << red << green << blue;
			yuv2rgb(y1, u, v, red, green, blue);
			ofs << red << green << blue;
		}
	}
	ofs.close();
}

double
Camera::computeImageMean(const Image& image) {
	double result = 0.0;
	// only get Y component
	const std::uint8_t* data = image.buffer.get();
	for (std::uint32_t i = 0; i < image.width * image.height; i++, data += 2) {
		result += *data;
	}
	return result / (image.width * image.height);
}

std::ostream&
operator<<(std::ostream& os, const Camera& camera) {
	return os << "{device: " << camera._device << ", format: " << camera.format()
	          << ", buffer_number: " << camera.ringBufferSize() << "}";
}
