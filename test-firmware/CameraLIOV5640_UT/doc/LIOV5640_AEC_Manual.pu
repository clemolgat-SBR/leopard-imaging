@startuml
autonumber
hide footbox
actor "Program" as P
participant "V4L2" as V
participant "Bridge CX3" as B
participant "Camera OV5640" as C
activate P

== Initialization ==

P -> V: OpenDevice(/dev/video0)
activate V
V -> B
activate B
B -> C
activate C
C --> B
B --> V
V --> P

P -> V: InitDevice()
V -> B
B -> C
C --> B
B --> V
V --> P

== Test ==

P -> V: StartStream()
V -> B
B -> C
C --> B
B --> V
V --> P

group "Get Auto Exposure Image Mean"
  loop "40 times"
    P -> V: GetImage()
    V -> B
    B -> C
    C --> B: image
    B --> V: image
    V --> P: image
  end
  P -> P: Compute Ref Mean on images [30,40[
end

P -> V: StopStream()
V -> B
B -> C
C --> B
B --> V
V --> P

P -> V: setManualExposure()
V -> B
B -> C
C --> B
B --> V
V --> P

group "Get Short Exposure Image Mean"
  P -> V: setExposure(valueLow)
  V -> B: valueLow
  B -> C: valueLow
  C --> B
  B --> V
  V --> P
  P -> V: StartStream()
  V -> B
  B -> C
  C --> B
  B --> V
  V --> P
  loop "40 times"
    P -> V: GetImage()
    V -> B
    B -> C
    C --> B: image
    B --> V: image
    V --> P: image
  end
  P -> P: Compute "LowMean" on last image
end

P -> V: StopStream()
V -> B
B -> C
C --> B
B --> V
V --> P

group "Get Long Exposure Image Mean"
  P -> V: setExposure(valueHigh)
  V -> B: valueHigh
  B -> C: valueHigh
  C --> B
  B --> V
  V --> P
  P -> V: StartStream()
  V -> B
  B -> C
  C --> B
  B --> V
  V --> P
  loop "40 times"
    P -> V: GetImage()
    V -> B
    B -> C
    C --> B: image
    B --> V: image
    V --> P: image
  end
  P -> P: Compute "HighMean" on last image
end

P -> P: Verify LowMean < HighMean

== Destruction ==

P -> V: StopStream()
V -> B
B -> C
C --> B
B --> V
V --> P

P -> V: CloseDevice()
V -> B
B -> C
C --> B
destroy C
B --> V
destroy B
V --> P
destroy V
destroy P
@enduml
