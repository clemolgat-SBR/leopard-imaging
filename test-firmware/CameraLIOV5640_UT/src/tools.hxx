//! @file
#pragma once

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern bool g_enableDump;

class SingleFormatAFTest : public ::testing::TestWithParam<FormatAF> {};

//! @test Checks CameraLIOV5640 latency to get the first Image.
TEST_P(SingleFormatAFTest, StartLatency) {
	SCOPED_TRACE("Test Start Latency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());
	// Run one time the camera to avoid the latency involved by the switch
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Start Stream and Get the First Frame");
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	ASSERT_NO_THROW(cam.start());
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "[ INFO     ] StartLatency, " << cam.device() << ", "
	          << std::to_string(GetParam()) << ", " << duration.count() << std::endl;
	EXPECT_LE(duration, std::chrono::milliseconds(1500))
	  << "[ ERROR    ] StartTooLong, " << cam.device() << ", "
	  << std::to_string(GetParam()) << ", " << duration.count();
}

//! @test Checks CameraLIOV5640 Image corruption using internal test pattern.
TEST_P(SingleFormatAFTest, CheckTestPattern) {
	SCOPED_TRACE("Test Image Test Pattern for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV5640PatternStream(
	  "FormatCheckTestPattern_" + std::to_string(GetParam()), cam, GetParam(), false);
}

//! @test Checks CameraLIOV5640 Image corruption using internal test pattern while
//! Images are drop.
TEST_P(SingleFormatAFTest, CheckTestPatternWithDrop) {
	SCOPED_TRACE("Test Image Test Pattern with Drop for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV5640PatternStream(
	  "FormatCheckTestPatternWithDrop_" + std::to_string(GetParam()),
	  cam,
	  GetParam(),
	  true);
}

//! @test Checks CameraLIOV5640 streaming stability.
TEST_P(SingleFormatAFTest, CheckImage) {
	SCOPED_TRACE("Test Image for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV5640Stream(
	  "FormatCheckImage_" + std::to_string(GetParam()), cam, GetParam(), false);
}

//! @test Checks CameraLIOV5640 streaming stability while Images are drop.
TEST_P(SingleFormatAFTest, CheckImageWithDrop) {
	SCOPED_TRACE("Test Image for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV5640Stream(
	  "FormatCheckImageWithDrop_" + std::to_string(GetParam()), cam, GetParam(), true);
}

//! @test Checks CameraLIOV5640 periods consistency between two consecutive Images.
TEST_P(SingleFormatAFTest, Regularity) {
	SCOPED_TRACE("Test Period for: " + std::to_string(GetParam()));
	std::int32_t expectedDelta = 1000 / GetParam().first.frameRate;
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	std::uint32_t previousSeq = img->seq;
	std::int32_t previousTime = img->seconds * 1000 + img->milliseconds;

	SCOPED_TRACE(cam.device() + ": Get " + std::to_string(IMG_LOOP) + " Frames");
	for (std::size_t i = 1; i <= IMG_LOOP; ++i) {
		SCOPED_TRACE(cam.device() + ": loop: " + std::to_string(i));
		ASSERT_NO_THROW(img = cam.getImage())
		  << "[ ERROR    ] NoImage, " << cam.device() << ", " << std::to_string(GetParam())
		  << ", " << std::to_string(i);
		// Mean we didn't DQBUF enough quickly.
		EXPECT_EQ(img->seq, previousSeq + 1);

		std::int32_t currentTime  = img->seconds * 1000 + img->milliseconds;
		std::int32_t currentDelta = currentTime - previousTime;
		SCOPED_TRACE(cam.device() + ": Frames " + std::to_string(previousSeq) + ":" +
		             std::to_string(previousTime) + "ms, " + std::to_string(img->seq) +
		             ": " + std::to_string(currentTime) + "ms, Expected time: " +
		             std::to_string(previousTime + expectedDelta) + "ms");
		SCOPED_TRACE(cam.device() + ": Current Delta: " + std::to_string(currentDelta) +
		             "ms, Expected Delta: " + std::to_string(expectedDelta) + "ms");
		std::cout << "[ INFO     ] Delta, " << cam.device() << ", "
		          << std::to_string(GetParam()) << ", " << std::to_string(currentDelta)
		          << std::endl;

		// 5ms jitter max between two consecutive image according to current format (i.e.
		// fps).
		EXPECT_TRUE(std::abs(currentDelta - expectedDelta) <= 5)
		  << ((img->seq == previousSeq + 1) ? "[ ERROR    ] FrameDrop, " :
		                                      "[ ERROR    ] MissFrame, ")
		  << cam.device() << ", " << std::to_string(GetParam()) << ", "
		  << std::to_string(img->seq);
		previousSeq  = img->seq;
		previousTime = currentTime;
	}
	if (g_enableDump) {
		std::stringstream ss;
		ss << "FormatRegularity_" << std::to_string(GetParam()) << ".ppm";
		SCOPED_TRACE(cam.device() + ": Write image " + ss.str());
		EXPECT_NO_THROW(cam.writeImage2PPM(*img, ss.str()));
	}

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Close Device");
	ASSERT_NO_THROW(cam.closeDevice());
}

//! @brief Stores a pair of FormatAF to check format change.
using FormatAFToFormatAF = std::pair<FormatAF, FormatAF>;
namespace std {
//! @brief Converts FormatAFToFormatAF value to std::string.
//! @param[in] in The value to convert.
//! @return the value converted.
std::string
to_string(const FormatAFToFormatAF& in) {
	return std::to_string(in.first) + "->" + std::to_string(in.second);
}
} // namespace std

class SwitchFormatAFTest : public ::testing::TestWithParam<FormatAFToFormatAF> {};

//! @test Checks CameraLIOV5640 latency to get the first Image.
TEST_P(SwitchFormatAFTest, StartLatency) {
	SCOPED_TRACE("Test Start Latency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam().first);
	// Run one time the camera with the previous format to compute the latency involved by
	// the switch
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.stop());

	// Run with the Second Format
	SCOPED_TRACE(cam.device() +
	             (GetParam().second.second == true ? ": Enable" : ": Disable") +
	             " AutoFocus");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	SCOPED_TRACE(cam.device() +
	             ": Set Second Format: " + std::to_string(GetParam().second.first));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));

	SCOPED_TRACE(cam.device() + ": Start Stream and Get the First Frame");
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	ASSERT_NO_THROW(cam.start());
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	const FormatAF& formatFirst  = GetParam().first;
	const FormatAF& formatSecond = GetParam().second;
	std::cout << "[ INFO     ] StartLatency, " << cam.device() << ", "
	          << std::to_string(formatFirst) << ", " << std::to_string(formatSecond)
	          << ", " << duration.count() << std::endl;
	EXPECT_LE(duration, std::chrono::milliseconds(1500))
	  << "[ ERROR    ] StartTooLong, " << cam.device() << ", "
	  << std::to_string(formatFirst) << ", " << std::to_string(formatSecond) << ", "
	  << duration.count();
}

//! @test Checks CameraLIOV5640 streaming stability.
TEST_P(SwitchFormatAFTest, CheckImage) {
	SCOPED_TRACE("Test Formats switch for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": run with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": run with the Second Format");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));

	testCameraLIOV5640Stream("FormatSwitch_" + std::to_string(GetParam()),
	                         cam,
	                         GetParam().first,
	                         GetParam().second,
	                         false);
}

//! @test Checks CameraLIOV5640 streaming stability while Images are drop.
TEST_P(SwitchFormatAFTest, CheckImageWithDrop) {
	SCOPED_TRACE("Test Formats switch with drop for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": run with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": run with the second Format");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));

	testCameraLIOV5640Stream("FormatSwitchWithDrop_" + std::to_string(GetParam()),
	                         cam,
	                         GetParam().first,
	                         GetParam().second,
	                         true);
}

//! @test Checks CameraLIOV5640 flip enable stability.
TEST_P(SwitchFormatAFTest, CheckFlipEnabledImage) {
	SCOPED_TRACE("Test flip consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Set Flip to 1 (UVC)");
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP, 1));
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP, 1));

	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Run Flip stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Run Flip stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror
}

//! @test Checks CameraLIOV5640 flip disable stability.
TEST_P(SwitchFormatAFTest, CheckFlipDisableImage) {
	SCOPED_TRACE("Test flip consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Set Flip to 0 (UVC)");
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP, 0));
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP, 0));

	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Run Flip stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Run Flip stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror
}

//! @test Checks CameraLIOV5640 AEC Auto stability.
TEST_P(SwitchFormatAFTest, CheckAECAutoImage) {
	SCOPED_TRACE("Test AEC Auto consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Enable AEC (UVC)");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO));

	SCOPED_TRACE(cam.device() + ": Check AEC is enable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_AUTO, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Run AEC stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_AUTO, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_AUTO, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_AUTO, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Run AEC stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_AUTO, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is enable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x3503) & 0x03);
}

//! @test Checks CameraLIOV5640 AEC Manual stability.
//! @todo Verify also Exposure Register stability.
TEST_P(SwitchFormatAFTest, CheckAECManualImage) {
	SCOPED_TRACE("Test AEC Manual consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Disable AEC (UVC)");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL));

	SCOPED_TRACE(cam.device() + ": Check AEC is disable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_MANUAL, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (Register)");
	EXPECT_EQ(0x3, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Run AEC stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_MANUAL, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (Register)");
	EXPECT_EQ(0x3, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_MANUAL, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (Register)");
	EXPECT_EQ(0x3, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_MANUAL, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (Register)");
	EXPECT_EQ(0x3, cam.readRegister(0x3503) & 0x03);

	SCOPED_TRACE(cam.device() + ": Run AEC stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (UVC)");
	EXPECT_EQ(V4L2_EXPOSURE_MANUAL, cam.getParameter(V4L2_CID_EXPOSURE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AEC is disable (Register)");
	EXPECT_EQ(0x3, cam.readRegister(0x3503) & 0x03);
}

//! @test Checks CameraLIOV5640 AWB Auto stability.
TEST_P(SwitchFormatAFTest, CheckAWBAutoImage) {
	SCOPED_TRACE("Test AWB Auto consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Enable AWB (UVC)");
	ASSERT_NO_THROW(
	  cam.setParameter(V4L2_CID_AUTO_WHITE_BALANCE, V4L2_WHITE_BALANCE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check AWB is enable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_AUTO, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is enable (Register)");
	EXPECT_EQ(0x1, cam.readRegister(0x5001) & 0x1);

	// Run one time the camera with the First FormatAF
	SCOPED_TRACE(cam.device() + ": Run AWB stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AWB is enable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_AUTO, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is enable (Register)");
	EXPECT_EQ(0x1, cam.readRegister(0x5001) & 0x1);
	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));

	SCOPED_TRACE(cam.device() + ": Run AWB stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());

	SCOPED_TRACE(cam.device() + ": Check AWB is enable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_AUTO, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is enable (Register)");
	EXPECT_EQ(0x1, cam.readRegister(0x5001) & 0x1);
}

//! @test Checks CameraLIOV5640 AWB Manual stability.
//! @todo Verify also Temperature Register stability.
TEST_P(SwitchFormatAFTest, CheckAWBManualImage) {
	SCOPED_TRACE("Test AWB Manual consistency for: " + std::to_string(GetParam()));
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() +
	             ": Set the First Format: " + std::to_string(GetParam().first));
	setupCameraLIOV5640(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": Disable AWB (UVC)");
	ASSERT_NO_THROW(
	  cam.setParameter(V4L2_CID_AUTO_WHITE_BALANCE, V4L2_WHITE_BALANCE_MANUAL));
	SCOPED_TRACE(cam.device() + ": Check AWB is disable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_MANUAL, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is disable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x5001) & 0x1);

	// Run one time the camera with the First FormatAF
	SCOPED_TRACE(cam.device() + ": Run AWB stream with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": Check AWB is disable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_MANUAL, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is disable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x5001) & 0x1);
	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() +
	             ": Set the Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_FOCUS_AUTO, GetParam().second.second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second.first));

	SCOPED_TRACE(cam.device() + ": Run AWB stream with the Second Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());

	SCOPED_TRACE(cam.device() + ": Check AWB is disable (UVC)");
	EXPECT_EQ(V4L2_WHITE_BALANCE_MANUAL, cam.getParameter(V4L2_CID_AUTO_WHITE_BALANCE));
	SCOPED_TRACE(cam.device() + ": Check AWB is disable (Register)");
	EXPECT_EQ(0x0, cam.readRegister(0x5001) & 0x1);
}
