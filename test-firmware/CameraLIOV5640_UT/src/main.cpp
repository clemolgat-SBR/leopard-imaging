//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <Camera.hpp>
#include <Tools.hpp>
#include <iostream>

//! @brief Default device file of the camera.
std::string g_device = "/dev/video0";
//! @brief Default Format to use.
Camera::Format g_preference = {{640, 480}, 15};
//! @brief Enable camera verbose log.
bool g_enableVerbose = false;
//! @brief Enable dump image.
bool g_enableDump = false;
//! @brief Default Format and Focus mode to use.
FormatAF g_format = {{{640, 480}, 30}, true};

//! @brief Display short help of the program.
//! @param[in] programName Name of the program (i.e. argv[0]).
static inline void
getUsage(const std::string& programName) {
	std::cout << "usage: " << programName << " -d /dev/video0 [...]" << std::endl
	          << "-h, --help: Prints the help message." << std::endl
	          << "-d, --device <dev>: device name (default /dev/video0)." << std::endl
	          << "--width <val>: width (default 640)." << std::endl
	          << "--height <val>: height (default 480)." << std::endl
	          << "--fps <val>: framerate (default 15)." << std::endl
	          << "--dump: activate image dump (default off)." << std::endl
	          << "-v, --verbose: activate verbose log (default off)." << std::endl;
}

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int
main(int argc, char** argv) {
	std::cout << "Test Plan Version: 1.0.5" << std::endl;
	// Help Usage
	if (isCmdOptionExists(argc, argv, "-h") || isCmdOptionExists(argc, argv, "--help")) {
		getUsage(argv[0]);
		::testing::InitGoogleTest(&argc, argv);
		return RUN_ALL_TESTS();
	}

	if (isCmdOptionExists(argc, argv, "-d")) {
		g_device = getCmdOption(argc, argv, "-d");
	} else if (isCmdOptionExists(argc, argv, "--device")) {
		g_device = getCmdOption(argc, argv, "--device");
	}

	if (isCmdOptionExists(argc, argv, "--width")) {
		g_preference.resolution.width =
		  std::atoi(getCmdOption(argc, argv, "--width").c_str());
	}
	if (isCmdOptionExists(argc, argv, "--height")) {
		g_preference.resolution.height =
		  std::atoi(getCmdOption(argc, argv, "--height").c_str());
	}
	if (isCmdOptionExists(argc, argv, "--fps")) {
		g_preference.frameRate = std::atoi(getCmdOption(argc, argv, "--fps").c_str());
	}

	g_format = FormatAF(g_preference, true);

	if (isCmdOptionExists(argc, argv, "--dump")) {
		g_enableDump = true;
	}
	if (isCmdOptionExists(argc, argv, "--verbose") ||
	    isCmdOptionExists(argc, argv, "-v")) {
		g_enableVerbose = true;
	}

	// Get GTest options
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
