//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 Auto Exposure info are correct.
TEST(AEC, CheckAECAutoInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Auto Exposure Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_EXPOSURE_AUTO));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 1);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, V4L2_EXPOSURE_AUTO);
}

//! @test Checks CameraLIOV5640 Exposure info are correct.
TEST(AEC, CheckAECExposureInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Exposure Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_EXPOSURE_ABSOLUTE));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 1048575);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 512);
}

//! @test Checks CameraLIOV5640 convergence take up to 15 frames when AEC enabled.
TEST(AEC, VerifyConvergence) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	ASSERT_NO_THROW(cam.flushBuffers());

	SCOPED_TRACE(cam.device() + ": Get 40 Frames");
	std::vector<std::unique_ptr<Camera::Image>> images;
	for (int i = 0; i < 40; ++i) {
		SCOPED_TRACE(cam.device() + ": Get Image " + std::to_string(i));
		std::unique_ptr<Camera::Image> img;
		EXPECT_NO_THROW(img = cam.getImage());
		if (img) {
			SCOPED_TRACE(cam.device() + ": Means: " +
			             std::to_string(CameraLIOV5640::computeImageMean(*img)));
			images.push_back(std::move(img));
		}
	}
	if (g_enableVerbose) {
		for (auto const& it : images) {
			std::cout << "(VERBOSE) Frame: " << it->seq << " ts: " << it->seconds << "s,"
			          << it->milliseconds << "ms"
			          << " Means: " << CameraLIOV5640::computeImageMean(*it.get())
			          << std::endl;
		}
	}
	// Compute Reference
	ASSERT_TRUE(images.size() >= 10);
	double meanReference = 0.0;
	for (auto it = images.end() - 10; it != images.end(); ++it) {
		meanReference += CameraLIOV5640::computeImageMean(**it);
	}
	meanReference = meanReference / 10.0;
	if (g_enableVerbose) {
		std::cout << "(VERBOSE) Reference Means based on last ten Frames: "
		          << std::to_string(meanReference);
	}
	SCOPED_TRACE(cam.device() + ": Reference Means based on last ten Frames: " +
	             std::to_string(meanReference));

	// Start at 15th Buffer AEC must have converged.
	for (const auto& img : images) {
		if (img->seq >= 15) {
			double frameMeanReference = CameraLIOV5640::computeImageMean(*img.get());
			SCOPED_TRACE(cam.device() + ": Frame " + std::to_string(img->seq) +
			             " Means: " + std::to_string(frameMeanReference));
			EXPECT_TRUE((meanReference + 2) > frameMeanReference);
			EXPECT_TRUE((meanReference - 2) < frameMeanReference);
		}
	}

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Close Device");
	ASSERT_NO_THROW(cam.closeDevice());
}

//! @test Checks CameraLIOV5640 manual exposure is working.
TEST(AEC, TestManualExposure) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	ASSERT_NO_THROW(cam.flushBuffers());
	// Wait for Stabilized AEC
	SCOPED_TRACE(cam.device() + ": Drop 40 Frames");
	for (std::size_t i = 0; i < 40; ++i) {
		SCOPED_TRACE(cam.device() + ": Get Image " + std::to_string(i));
		ASSERT_NO_THROW(cam.getImage());
	}
	double meanAuto = CameraLIOV5640::computeImageMean(*cam.getImage().get());
	SCOPED_TRACE("Y reference mean: " + std::to_string(meanAuto));
	SCOPED_TRACE("Set Auto Exposure to manual.");
	SCOPED_TRACE("Set very low manual exposure time.");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL));
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_ABSOLUTE, 200)); // dark
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_GAIN, 0));
	SCOPED_TRACE(cam.device() + ": Drop 40 Frames");
	for (std::size_t i = 0; i < 40; ++i) {
		SCOPED_TRACE(cam.device() + ": Get Image " + std::to_string(i));
		ASSERT_NO_THROW(cam.getImage());
	}
	double meanManual = CameraLIOV5640::computeImageMean(*cam.getImage().get());
	SCOPED_TRACE("Y mean: " + std::to_string(meanManual));
	EXPECT_TRUE(meanManual < meanAuto - 20);

	// Set Manual before Stream On
	{
		SCOPED_TRACE(cam.device() + ": Stop Stream");
		ASSERT_NO_THROW(cam.stop());
		SCOPED_TRACE("Set Auto Exposure to manual.");
		SCOPED_TRACE("Set very low manual exposure time.");
		ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL));
		ASSERT_NO_THROW(cam.setParameter(V4L2_CID_EXPOSURE_ABSOLUTE, 200));
		ASSERT_NO_THROW(cam.setParameter(V4L2_CID_GAIN, 0));
		SCOPED_TRACE(cam.device() + ": Start Stream");
		ASSERT_NO_THROW(cam.start());
		SCOPED_TRACE(cam.device() + ": Flush...");
		ASSERT_NO_THROW(cam.flushBuffers());
		SCOPED_TRACE("Drop 40 images...");
		for (std::size_t i = 0; i < 40; ++i) {
			SCOPED_TRACE(cam.device() + ": Get Image " + std::to_string(i));
			ASSERT_NO_THROW(cam.getImage());
		}
		double meanManual2 = CameraLIOV5640::computeImageMean(*cam.getImage().get());
		SCOPED_TRACE("Y mean: " + std::to_string(meanManual2));
		EXPECT_TRUE(meanManual2 < meanAuto - 20);
		EXPECT_TRUE(meanManual + 3 > meanManual2);
		EXPECT_TRUE(meanManual - 3 < meanManual2);
	}

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Close Device");
	ASSERT_NO_THROW(cam.closeDevice());
}
