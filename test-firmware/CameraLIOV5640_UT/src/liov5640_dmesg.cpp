//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 has not error reported on dmesg.
TEST(Dmesg, Dmesg) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam);
	SCOPED_TRACE(cam.device() + ": Open Device");
	ASSERT_NO_THROW(cam.openDevice(););

	ASSERT_NO_THROW(cam.setFormat({{640, 480}, 15}));
	cam.setParameter(V4L2_CID_FOCUS_AUTO,
	                 0); // disable auto focus -> will cause the error message !

	SCOPED_TRACE(cam.device() + ": Start");
	testCameraLIOV5640Stream("TestDmesg", cam, g_format, false);
	SCOPED_TRACE(cam.device() + ": Stop");
	ASSERT_NO_THROW(cam.stop());

	cam.setParameter(V4L2_CID_FOCUS_AUTO,
	                 1); // enable auto focus -> will cause the error message !
}
