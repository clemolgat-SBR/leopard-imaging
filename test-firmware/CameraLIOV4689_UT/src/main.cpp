//! @file
#include <gtest/gtest.h>

#include <Camera.hpp>
#include <Tools.hpp>
#include <iostream>

//! @brief Default device file of the camera.
std::string g_device = "/dev/video0";
//! @brief Default Format to use.
Camera::Format g_preference = {{2560, 720}, 15};
//! @brief Enable camera verbose log.
bool g_enableVerbose = false;
//! @brief Enable dump image.
bool g_enableDump = false;

//! @brief Display short help of the program.
//! @param[in] programName Name of the program (i.e. argv[0]).
static inline void
getUsage(const std::string& programName) {
	std::cout << "usage: " << programName << " -d /dev/video0 [...]" << std::endl
	          << "-h, --help: Prints the help message." << std::endl
	          << "-d, --device <dev>: device name (default /dev/video0)." << std::endl
	          << "--width <val>: width (default 2560)." << std::endl
	          << "--height <val>: height (default 720)." << std::endl
	          << "--fps <val>: framerate (default 15)." << std::endl
	          << "--dump: activate image dump (default off)." << std::endl
	          << "-v, --verbose: activate verbose log (default off)." << std::endl;
}

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int
main(int argc, char** argv) {
	std::cout << "Test Plan Version: 0.1" << std::endl;
	// Help Usage
	if (isCmdOptionExists(argc, argv, "-h") || isCmdOptionExists(argc, argv, "--help")) {
		getUsage(argv[0]);
		::testing::InitGoogleTest(&argc, argv);
		return RUN_ALL_TESTS();
	}

	if (isCmdOptionExists(argc, argv, "-d")) {
		g_device = getCmdOption(argc, argv, "-d");
	} else if (isCmdOptionExists(argc, argv, "--device")) {
		g_device = getCmdOption(argc, argv, "--device");
	}

	if (isCmdOptionExists(argc, argv, "--width")) {
		g_preference.resolution.width =
		  std::atoi(getCmdOption(argc, argv, "--width").c_str());
	}
	if (isCmdOptionExists(argc, argv, "--height")) {
		g_preference.resolution.height =
		  std::atoi(getCmdOption(argc, argv, "--height").c_str());
	}
	if (isCmdOptionExists(argc, argv, "--fps")) {
		g_preference.frameRate = std::atoi(getCmdOption(argc, argv, "--fps").c_str());
	}
	if (isCmdOptionExists(argc, argv, "--dump")) {
		g_enableDump = true;
	}
	if (isCmdOptionExists(argc, argv, "--verbose") ||
	    isCmdOptionExists(argc, argv, "-v")) {
		g_enableVerbose = true;
	}

	// Get GTest options
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
