[![build status](https://gitlab.com/clemolgat-SBR/leopard-imaging/badges/master/build.svg)](https://gitlab.com/clemolgat-SBR/leopard-imaging/commits/master)

# Description
This repository contains everything needed to use and test robot firmware.
from Leopard Imaging.  
We suppose you are using the latest [LTS Ubuntu](https://hub.docker.com/_/ubuntu/) host machine.  

You'll find in:
* **[NAO](nao/README.md)** Contains information for using NAO's head.
* **[pepper](pepper/README.md)** Contains information for using the robot head.
* **[toolchain](toolchain/README.md)** Documentation how to use qibuild and a toolchain to cross compile project for the robot head.
* **[flash-ov580](flash-ov580/README.md)** The project for LI-OV4689 Stereo Camera flasher.
* **[flash-cx3](flash-cx3/README.md)** The project for LI-OV5640 2D Camera flasher.
* **[test-firmware](test-firmware/README.md)** The project for Acceptance Test Plan for 2D Camera and Stereo Camera firmware and some toolings.
 * You can found Doxygen Documentation [here](https://clemolgat-sbr.gitlab.io/leopard-imaging/)

And voila !

