#! /usr/bin/env bash

set -e
read -e -p "Build flash-cx3 (Y/n)" BUILD_FLASH_CX3
read -e -p "Build flash-ov580 (Y/n)" BUILD_FLASH_OV580
read -e -p "Build test-firmware (Y/n)" BUILD_TEST_FIRMWARE
read -e -p "Build doc (Y/n)" BUILD_DOC

#set -x
#################
##  FLASH CX3  ##
#################
case ${BUILD_FLASH_CX3:0:1} in
    n|N )
       BUILD_FLASH_CX3=false
    ;;
    y|Y|* )
       BUILD_FLASH_CX3=true
    ;;
esac

if ${BUILD_FLASH_CX3}
then
	gitlab-runner exec docker flash-cx3:cmake
	gitlab-runner exec docker flash-cx3:qibuild
	gitlab-runner exec docker flash-cx3:robot
fi

###################
##  FLASH OV580  ##
###################
case ${BUILD_FLASH_OV580:0:1} in
    n|N )
       BUILD_FLASH_OV580=false
    ;;
    y|Y|* )
       BUILD_FLASH_OV580=true
    ;;
esac

if ${BUILD_FLASH_OV580}
then
	gitlab-runner exec docker flash-ov580:cmake
	gitlab-runner exec docker flash-ov580:qibuild
	gitlab-runner exec docker flash-ov580:robot
fi

###############
##  TEST FW  ##
###############
case ${BUILD_TEST_FIRMWARE:0:1} in
    n|N )
       BUILD_TEST_FIRMWARE=false
    ;;
    y|Y|* )
       BUILD_TEST_FIRMWARE=true
    ;;
esac

if ${BUILD_TEST_FIRMWARE}
then
	gitlab-runner exec docker test-firmware:cmake
	gitlab-runner exec docker test-firmware:qibuild
	gitlab-runner exec docker test-firmware:robot
fi

#####################
##  Documentation  ##
#####################
case ${BUILD_DOC:0:1} in
    n|N )
       BUILD_DOC=false
    ;;
    y|Y|* )
       BUILD_DOC=true
    ;;
esac

if ${BUILD_DOC}
then
	gitlab-runner exec docker pages
fi

