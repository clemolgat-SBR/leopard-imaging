#!/usr/bin/env bash

set -x
sudo apt-get update
sudo apt-get install -qq git build-essential python-pip cmake doxygen graphviz libusb-1.0-0-dev
sudo apt-get install -qq gstreamer0.10-tools gstreamer0.10-plugins-base gstreamer0.10-plugins-good
pip install qibuild --user
pip install --upgrade pip
