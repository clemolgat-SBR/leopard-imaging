# Description
Here how to create and use a robot cross toolchain on linux64. You'll need to follow this
step in order to compile program for the robot head and deploy it to test.

# Prerequisite
## qiBuild
First you need Aldebaran [qiBuild](http://doc.aldebaran.com/qibuild/index.html) using python installer PIP.
```sh
pip install --user qibuild
```
note: more info [here](http://doc.aldebaran.com/qibuild/beginner/getting_started.html)

Once, installed you could use:
```sh
qibuild --version
```
You should see something like:
```sh
qibuild version 3.11.6
Using Python code from /home/clemolgat/.local/lib/python2.7/site-packages
Using CMake code from /home/clemolgat/.local/share/cmake
```

## CMake 3.2
Project also use CMake >= 3.2, C++11, and v4l2 API (i.e. programs are linux only).
To install new cmake version on *old* Ubuntu just retrieve a new version on ppa.
```sh
sudo add-apt-repository ppa:george-edison55/cmake-3.x -y
sudo apt-get update -qq
sudo apt-get install -qq cmake
```

# Create a Cross toolchain and a Config
Qibuild manage it through *qitoolchain create* and *qibuild add-config*.
## Create qiBuild toolchain and config automatically
To get the toolchain archive, create the qitoolchain, and create a config, you
can use the script **install_toolchain.sh**
```sh
./install_toolchain.sh
```

## Create toolchain and config Manually
Otherwise by hand you can do  
1. Getting a toolchain for a Robot
To be able to build binary for a robot, first you need a "cross toolchain".
```
mkdir ~/ctc
tar xzvf ctc-linux64-atom-2.7.0.161.tar.gz -C ~/ctc
qitoolchain create robot-ctc ctc-linux64-atom-2.7.0.161/toolchain.xml
```
2. Create a qiBuild Configuration
Then we have to create a qibuild configuration which use the *robot-ctc* toolchain.
```
qibuild add-config -t robot-ctc robot
```

# Create a qiBuild worktree
Now you should have a toolchain and a build config ready, it's time to create a worktree and build a project !  
note: you can use **qitoolchain list** to see toolchain and **qibuild config** to
see config available.

In order to build a project, first you need to create a [worktree](http://doc.aldebaran.com/qibuild/beginner/qibuild/tutorial.html#creating-a-worktree).
```sh
mkdir ~/work && cd ~/work
qibuild init
```
This, should create a hidden directory ".qi" needed for building project.

Now you can, build project in the worktree.
```sh
cd ~/work
git clone git@github.com:aldebaran/leopard-imaging.git
cd leopard-imaging/flash-ov580
```

A project consist in a qiproject.xml and CMakeLists.txt file at its root
directory (e.g. flash-ov580 or test-firmware).

# Build
First go to the racine of the project e.g.
```sh
cd ~/work/leopard-imaging/flash-ov580
```

## Cross compiling for robot
Since qibuild is based on CMake you'll have the compilation in two phases (i.e. camke configure, make)
```sh
qibuild configure -c robot --release
qibuild make -c robot
```
## Deploying on Robot
Now, it's time to deploy the binaries on the robot head.  
Again, qibuild provide a tool...
```sh
qibuild deploy -c robot --url nao@ip.of.the.robot:~/foo
```
note: if password is needed it's "nao" (try to use ssh-copy-id nao@ip.of.your.robot to avoid it)

## Testing on Robot
On robot:
```sh
ssh nao@ip.of.the.robot
./foo/bin/<your_program>
```
